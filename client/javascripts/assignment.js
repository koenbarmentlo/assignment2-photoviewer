﻿(function () {
    "use strict"

    window.assignment = function () {
        window.assignment.ROWCOUNT = 4;
        window.assignment.COLUMNCOUNT = 4;
        window.assignment.WIDTH = 640;
        window.assignment.HEIGHT = 480;
        window.assignment.youtubeId = "LMLwslRNThM";
        window.assignment.hintsOn = false;
        assignment.applySettings();
        window.assignment.TILEWIDTH = Math.round(window.assignment.WIDTH / window.assignment.COLUMNCOUNT);
        window.assignment.TILEHEIGHT = Math.round(window.assignment.HEIGHT / window.assignment.ROWCOUNT);        
        window.assignment.tiles = [];
        YoutubeVideo(window.assignment.youtubeId, "http://server7.tezzt.nl:3000/api/proxy", function (video) {            
            assignment.appendVideo(video);
            assignment.initCanvasElements();
            assignment.randomizeArrayOrder(assignment.tiles);
            assignment.printCanvasElements();
            assignment.appendArrowKeysListener();
            assignment.videoElement.addEventListener('timeupdate', function () {
                for (var i = 0; i < assignment.tiles.length; i++) {
                    var currentTile = assignment.tiles[i];
                    var destWidth = currentTile.canvasElement.width;
                    var destHeight = currentTile.canvasElement.height;
                    var sourceWidth = document.querySelector("video").offsetWidth;
                    var sourceHeight = document.querySelector("video").offsetHeight;
                    var sX = currentTile.x * (sourceWidth / assignment.COLUMNCOUNT);
                    var sY = currentTile.y * (sourceHeight / assignment.ROWCOUNT);
                    var sWidth = sourceWidth / assignment.COLUMNCOUNT;
                    var sHeight = sourceHeight / assignment.ROWCOUNT                    
                    var canvasContext = currentTile.canvasElement.getContext('2d');
                    canvasContext.drawImage(assignment.videoElement, sX, sY, sWidth, sHeight, 0, 0, destWidth, destHeight);                    
                    if (assignment.hintsOn) {
                        canvasContext.fillText(currentTile.indexNumber.toString(), 40, 40);
                    }
                }
            }, false);
        });       
    }

    window.assignment.applySettings = function () {
        var settings = assignment.parseURLParameters();
        if (settings["rows"] !== undefined && !isNaN(parseInt(settings["rows"]))) {
            assignment.ROWCOUNT = parseInt(settings["rows"]);
        }
        if (settings["cols"] !== undefined && !isNaN(parseInt(settings["cols"]))) {
            assignment.COLUMNCOUNT = parseInt(settings["cols"]);
        }
        if (settings["hints"] !== undefined && !isNaN(parseInt(settings["hints"]))) {
            if (parseInt(settings["hints"]) === 0) {
                assignment.hintsOn = false;
            }
            else if (parseInt(settings["hints"]) === 1) {
                assignment.hintsOn = true;
            }
        }
        if (settings["ID"] !== undefined) {
            window.assignment.youtubeId = settings["ID"];
        }
    }

    window.assignment.appendVideo = function (video) {
        var webm = video.getSource("video/webm", "medium");
        var mp4 = video.getSource("video/mp4", "medium");
        assignment.videoElement = document.createElement('video');
        assignment.videoElement.classList.add("video");
        assignment.videoElement.setAttribute('controls', 'controls');
        assignment.videoElement.setAttribute('src', webm.url);
        assignment.videoElement.setAttribute('autoplay', '');
        assignment.videoElement.loop = true;
        assignment.videoElement.style.width = assignment.WIDTH;
        assignment.videoElement.style.height = assignment.HEIGHT;
        assignment.videoElement.volume = 1;
        document.body.appendChild(assignment.videoElement);       
    }

    window.assignment.initCanvasElements = function () {
        assignment.tilesElement = document.createElement('div');
        assignment.tilesElement.classList.add('tiles');
        assignment.tilesElement.style.width = assignment.WIDTH + "px";
        assignment.tilesElement.style.height = assignment.HEIGHT + "px";
        var index = 0;
        for (var y = 0; y < assignment.ROWCOUNT; y++) {
            for (var x = 0; x < assignment.COLUMNCOUNT; x++) {
                var canvasElement = document.createElement("canvas");
                canvasElement.setAttribute("class", "tile");
                canvasElement.style.width = assignment.TILEWIDTH + "px";
                canvasElement.style.height = assignment.TILEHEIGHT + "px";
                var empty = false;
                if (x === assignment.COLUMNCOUNT - 1 && y === assignment.ROWCOUNT - 1) {
                    empty = true;
                    canvasElement.style.visibility = "hidden";
                }
                canvasElement.id = "tile" + index;
                canvasElement.getContext('2d').fillStyle = '#f00';
                canvasElement.getContext('2d').font = 'italic bold 30px sans-serif';
                canvasElement.getContext('2d').textBaseline = 'baseline';
                assignment.tiles.push(assignment.createTile(empty, index, canvasElement, x, y));
                index++;
            }
        }
        document.body.appendChild(assignment.tilesElement);
    }

    window.assignment.randomizeArrayOrder = function(array) {
        var currentIndex = array.length
        var randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            assignment.swapArrayItems(array, currentIndex, randomIndex);
        }
    }

    window.assignment.printCanvasElements = function () {
        assignment.tilesElement.innerHtml = "";
        for (var i = 0; i < assignment.tiles.length; i++) {
            assignment.tilesElement.appendChild(assignment.tiles[i].canvasElement);
        }
    }

    window.assignment.moveTile = function(oldIndex, newIndex) {        
        if (assignment.isValidSwap(oldIndex, newIndex)) {
            assignment.swapArrayItems(assignment.tiles, newIndex, oldIndex);
            assignment.printCanvasElements();
            assignment.hasWon();
        }                
    }

    window.assignment.appendArrowKeysListener = function () {
        document.addEventListener("keydown", function (e) {
            var emptyTileArrayIndex = assignment.getEmptyTileArrayIndex();
            if (e.keyCode === 37) { //Left
                assignment.moveTile(emptyTileArrayIndex, emptyTileArrayIndex - 1);
            }
            else if (e.keyCode === 38) { //Up
                assignment.moveTile(emptyTileArrayIndex, emptyTileArrayIndex - assignment.COLUMNCOUNT);
            }
            else if (e.keyCode === 39) { //Right
                assignment.moveTile(emptyTileArrayIndex, emptyTileArrayIndex + 1);
            }
            else if (e.keyCode === 40) { // Down
                assignment.moveTile(emptyTileArrayIndex, emptyTileArrayIndex + assignment.COLUMNCOUNT);
            }
            else if (e.keyCode == 33) { // Page up
                //volume up
                if (assignment.videoElement.volume !== 1.0) {
                    assignment.videoElement.volume = assignment.videoElement.volume + 0.1;
                }                
            }
            else if (e.keyCode == 34) { // Page down
                //volume down
                if (assignment.videoElement.volume >= 0.1) {
                    assignment.videoElement.volume = assignment.videoElement.volume - 0.1;
                }
            }
            else if (e.keyCode == 72) { //h
                assignment.hintsOn = !assignment.hintsOn;
            }
            else if (e.keyCode == 77) { //m
                assignment.videoElement.volume = 0;
            }
            else if (e.keyCode == 19) { //pause/break
                if (assignment.videoElement.paused) {
                    assignment.videoElement.play();
                }
                else {
                    assignment.videoElement.pause();
                }
            }
        });
    }

    window.assignment.onTileClick = function (e) {
        var id = parseInt(e.currentTarget.id.replace("tile", ""));
        var arrayIndex = assignment.findTileArrayIndexByID(id);
        var emptyTileIndex = assignment.getEmptyTileArrayIndex();
        window.assignment.moveTile(arrayIndex, emptyTileIndex);
    }

    window.assignment.isValidSwap = function (oldIndex, newIndex) {
        if (assignment.tiles[oldIndex] === undefined || assignment.tiles[newIndex] === undefined) {
            return false;
        }
        var x1 = oldIndex % assignment.COLUMNCOUNT;
        var x2 = newIndex % assignment.COLUMNCOUNT;
        var horizontal = (Math.abs(oldIndex - newIndex) === 1);
        var vertical = (Math.abs(oldIndex - newIndex) === assignment.COLUMNCOUNT);
        if (!(horizontal || vertical)) {
            return false;
        }
        if (horizontal) {
            if ((x1 > 1 && x2 === 0) || (x2 > 1 && x1 === 0)) {
                return false;
            }
        }
        return true;
    }

    window.assignment.getEmptyTileArrayIndex = function() {
        for (var i = 0; i < assignment.tiles.length; i++) {
            if(assignment.tiles[i].empty === true) {
                return i;
            }
        }
    }    

    window.assignment.swapArrayItems = function (array, id1, id2) {
        var item = array[id2];
        array[id2] = array[id1];
        array[id1] = item;
    }
    
    window.assignment.createTile = function(_empty, _indexNumber, _canvasElement, _x, _y) {
        var tile = {
            x: _x,
            y: _y,
            empty: _empty,
            indexNumber: _indexNumber,
            canvasElement: _canvasElement,
        }
        tile.canvasElement.addEventListener("click", assignment.onTileClick);
        return tile;
    }
    
    window.assignment.findTileArrayIndexByID = function(id) {
        for (var i = 0; i < window.assignment.tiles.length; i++) {
            if (assignment.tiles[i].indexNumber === id) {
                return i;
            }            
        }       
    }

    window.assignment.hasWon = function () {
        for (var i = 0; i < assignment.tiles.length-1; i++) {
            var id1 = assignment.tiles[i + 1].indexNumber;
            var id2 = assignment.tiles[i].indexNumber;
            if (id1 < id2) {
                return false;
            }
        }        
        var message = document.createElement("p");
        message.innerText = "You won!";
        document.body.appendChild(message);
        return true;
    }
    
    window.assignment.parseURLParameters = function() {
        var urlParameters = {};
        var parameters = document.location.href.split("?")[1];
        if (parameters !== undefined) {
            var urlargs = parameters.split("&");
            for (var i = 0; i < urlargs.length; i++) {
                var pair = urlargs[i].split("=");
                urlParameters[pair[0]] = pair[1];
            }
        }
        return urlParameters;
    }
}());
window.addEventListener("load", assignment);